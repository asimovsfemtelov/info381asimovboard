package entities;

import ethicalLayer.PredictionModule;
import pathing.AStarPath;
import world.Board;
import world.Lava;
import world.Tile;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * AsimovRobot is a representation of the robot used in the simulation.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class AsimovRobot extends JLabel implements Entity {

    private Point position;
    private java.util.List<Tile> path;
    private java.util.List<Human> humansObserved;
    private PredictionModule predictionModule;
    private Tile[][] tiles;
    private int goalX, goalY;
    private boolean goToOriginal = false;

    /**
     * Constructor for AsimovRobot.
     *
     * @param name - The name which will be shown on the board.
     * @param tiles - The tiles from 2D borad.
     * @param goalX - Original goal X coordinate.
     * @param goalY - Original goal Y coordinate.
     */
    public AsimovRobot(String name, Tile[][] tiles, int goalX, int goalY){
        super(name);
        position = new Point(0, 0);
        this.tiles = tiles;
        predictionModule = new PredictionModule(tiles);
        this.goalX = goalX; this.goalY = goalY;
    }

    /**
     * Evaluates the situation to observe status of humans.
     */
    public void evaluate(){
        humansObserved = getAllHumans(tiles);
        for(Human human : humansObserved) {
            int x = predictionModule.evaluateDanger(human, this);
            if(x == 1 || x == 2){
                path = predictionModule.generateGoal(human.getPosition());
                goToOriginal = false;
                break;
            } else if ((x == 3 || x == 4) && !goToOriginal) {
                System.out.println("ROBOT - Going to original goal! - " + goalX + ", " + goalY);
                AStarPath aStarPath = new AStarPath(goalX, goalY, tiles);
                path = aStarPath.aStar((int) position.getX(), (int) position.getY(), Board.getLavaTiles());
                goToOriginal = true;
            }
        }
    }

    /**
     * Finds all humans on the board.
     *
     * @param tiles - tiles from 2D board.
     * @return list of all humans.
     */
    public java.util.List<Human> getAllHumans(Tile[][] tiles){
        java.util.List<Human> humans = new ArrayList<>();
        for(int x = 0; x < Board.ROWS; x++){
            for(int y = 0; y < Board.COLUMNS; y++){
                for(Component component : tiles[x][y].getComponents()){
                    if(component instanceof Human){
                        humans.add((Human) component);
                    }
                }
            }
        }
        return humans;
    }

    /**
     * Follows to path a goal.
     *
     * @param tiles - tiles from 2D board.
     */
    public void followPath(Tile[][] tiles){
        if(!path.isEmpty() && path != null) {
            moveTo(tiles, path.get(0).getxPos(), path.get(0).getyPos());
            path.remove(0);
        }
    }


    /**
     * Moves to target position, given that the position is not out of bounds or containing lava.
     *
     * @param tileHolder - tiles from 2D board.
     * @param targetX - X target coordinate.
     * @param targetY - Y target coordinate.
     */
    @Override
    public void moveTo(Tile[][] tileHolder, int targetX, int targetY) {
        if((targetX < Board.ROWS && targetX >= 0 && targetY < Board.COLUMNS && targetY >= 0) &&
                !(tileHolder[targetX][targetY] instanceof Lava)){
            if(((position.x + 1 == targetX || position.x - 1 == targetX) && targetY == position.y) ||
                    ((position.y + 1 == targetY || position.y - 1 == targetY) && targetX == position.x)) {
                setPosition(targetX, targetY);
                tileHolder[targetX][targetY].add(this);
            } else {
                System.out.println("Illegal move");
            }
        } else {
            System.out.println("Something in the way!");
        }
    }

    /**
     * Saves human if in range.
     */
    public void saveHuman(){
        humansObserved = getAllHumans(tiles);
        for(Human human : humansObserved){ // not an excellent solution for multiple humans.
            predictionModule.humanInRange(human);
        }
    }


    /**
     * Getter for position.
     *
     * @return position of robot
     */
    @Override
    public Point getPosition() {
        return position;
    }

    /**
     * Setter for position.
     *
     * @param x - X coordinate for position.
     * @param y - Y coordinate for position-
     */
    @Override
    public void setPosition(int x, int y) {
        position.setLocation(x, y);
    }

    /**
     * Setter for path.
     *
     * @param path - Path as a list.
     */
    public void setPath(List<Tile> path) {
        this.path = path;
    }

    /**
     * Getter for goalX.
     *
     * @return goal X coordinate.
     */
    public int getGoalX(){
        return goalX;
    }

    /**
     * Getter for goalY.
     *
     * @return goal Y coordinate.
     */
    public int getGoalY(){
        return goalY;
    }
}

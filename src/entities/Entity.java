package entities;

import world.Tile;

import javax.swing.*;
import java.awt.*;

/**
 * Entity is an interface for all entities used in the simulation.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public interface Entity {
    void moveTo(Tile[][] tiles, int x, int y);
    Point getPosition();
    void setPosition(int x, int y);
}

package entities;

import pathing.Direction;
import world.Board;
import world.Lava;
import world.Tile;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Human is a representation of the human used in the simulation.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class Human extends JLabel implements Entity {

    private Point position;
    private List<Tile> path;
    private boolean dead, saved;
    private Direction facing;
    private int goalX, goalY;

    /**
     * Constructor for Human.
     *
     * @param name - The name which will be shown on the board.
     * @param goalX - Goal X coordinate.
     * @param goalY - Goal Y coordinate.
     */
    public Human(String name, int goalX, int goalY){
        super(name);
        position = new Point(0, 0);
        this.goalX = goalX; this.goalY = goalY;
        dead = false;
        saved = false;
        facing =  Direction.EAST;
    }

    /**
     * Follows to path a goal.
     *
     * @param tiles - tiles from 2D board.
     */
    public void followPath(Tile[][] tiles){
        if(!path.isEmpty() && path != null) {
            moveTo(tiles, path.get(0).getxPos(), path.get(0).getyPos());
            path.remove(0);
        }
    }

    /**
     * Moves to target position, given that the position is not out of bounds.
     * Moving into lava is possible.
     *
     * @param tileHolder - tiles from 2D board.
     * @param targetX - X target coordinate.
     * @param targetY - Y target coordinate.
     */
    @Override
    public void moveTo(Tile[][] tileHolder, int targetX, int targetY) { // bør sikkert IKKE ha tileHolder som parameter!
        if(targetX < Board.ROWS && targetX >= 0 && targetY < Board.COLUMNS && targetY >= 0){ // DENNE BØR KANSKJE VÆRE PÅ NESTE LINJE FOR Å HINDRE OUT OF BOUNDS EXCEPTIONS
            if(((position.x + 1 == targetX || position.x - 1 == targetX) && targetY == position.y) ||
                    ((position.y + 1 == targetY || position.y - 1 == targetY) && targetX == position.x)) {

                if(position.y + 1 == targetY){
                    facing = Direction.EAST;
                } else if (position.y - 1 == targetY){
                    facing = Direction.WEST;
                } else if (position.x + 1 == targetX){
                    facing = Direction.SOUTH;
                } else if (position.x - 1 == targetX){
                    facing = Direction.NORTH;
                }

                setPosition(targetX, targetY);

                tileHolder[targetX][targetY].add(this);
                if(tileHolder[targetX][targetY] instanceof Lava){
                    dead = true;
                    System.out.println("Human died!");
                }
            } else {
                System.out.println("llegal move!");
            }
        } else {
            System.out.println("Something in the way!");
        }
    }

    /**
     * Getter for position.
     *
     * @return position of robot
     */
    @Override
    public Point getPosition() {
        return position;
    }

    /**
     * Setter for position.
     *
     * @param x - X coordinate for position.
     * @param y - Y coordinate for position.
     */
    @Override
    public void setPosition(int x, int y) {
        position.setLocation(x, y);
    }

    /**
     * Setter for path.
     *
     * @param path - Path as a list.
     */
    public void setPath(List<Tile> path) {
        this.path = path;
    }


    /**
     * Sets saved to true when human got saved.
     */
    public void gotSaved(){
        saved = true;
    }

    /**
     * Checks if human is saved.
     *
     * @return true or false if human is saved.
     */
    public boolean isSaved(){
        return saved;
    }

    /**
     * Checks if human is dead.
     *
     * @return true or false if human is dead.
     */
    public boolean isDead(){
        return dead;
    }

    /**
     * Getting the direction which the human is facing.
     *
     * @return the direction.
     */
    public Direction getFacing() {
        return facing;
    }

    /**
     * Setting the direction which the human is facing.
     *
     * @param facing the direction.
     */
    public void setFacing(Direction facing) {
        this.facing = facing;
    }

    /**
     * Getter for goalX.
     *
     * @return goal X coordinate.
     */
    public int getGoalX(){
        return goalX;
    }

    /**
     * Getter for goalY.
     *
     * @return goal Y coordinate.
     */
    public int getGoalY(){
        return goalY;
    }
}

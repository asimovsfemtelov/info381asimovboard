package ethicalLayer;

import entities.AsimovRobot;
import entities.Human;
import pathing.AStarPath;
import utils.ManhattanDistance;
import pathing.Direction;
import world.Tile;

import java.awt.*;
import world.Board;
import world.Lava;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * The PredictionModule is responsible of evaluating levels of danger and generating
 * new goals to intervene humans heading for lava. It is only used by the robot.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class PredictionModule {
    private HashSet<Tile> lava;
    private int LOWDANGERDISTANCE = 6;
    private int HIGHDANGERDISTANCE = 4;
    private Tile nearestLava;
    private Boolean humanInDanger = false;
    private Tile[][] tiles;
    private Point robotPosition;

    /**
     * Constructor for PredictionModule.
     *
     * @param tiles - tiles from 2D board.
     */
    public PredictionModule(Tile[][] tiles){
        this.tiles = tiles;
        lava = Board.getLavaTiles();
    }

    /**
     * Checks if human is in danger, returns appropriate action for the robot.
     *
     * @param human - the human which gets evaluated by robot.
     * @return 0 when human is safe, 1 if it's in high danger, 2 if it's in low danger, 3 if it has been saved and 4 if it's dead
     */
    public int evaluateDanger(Human human, AsimovRobot robot){
        double lavaDistance = 1000.0;
        this.robotPosition = robot.getPosition();
        for(Tile tile : lava){
            double tempDistance = ManhattanDistance.calc(tile, (int) human.getPosition().getX(), (int) human.getPosition().getY());
            if(tempDistance < lavaDistance){
                lavaDistance = tempDistance;
                nearestLava = tile;
                if(lavaDistance == 0){
                    humanInDanger = false;
                    return 4;
                } else if(human.isSaved()){
                    return 3;
                }
            }
        }

        if(lavaDistance <= HIGHDANGERDISTANCE && !human.isSaved()){
            humanInDanger = true;
            //highdanger!
            return 1;
        } else if (lavaDistance <= LOWDANGERDISTANCE && !human.isSaved()){
            humanInDanger = true;
            //lowdanger!
            return 2;
        }

        //safe to continue
        humanInDanger = false;
        return 0;
    }

    /**
     * Generates a new path for the robot to intercept the human.
     *
     * @param humanPosition - the human's position.
     * @return the list with the new path to follow
     */
    public List<Tile> generateGoal(Point humanPosition){
        int x = (int) humanPosition.getX();
        int y = (int) humanPosition.getY();

        Tile bestTile = generatePathCloseToLava(x, y);

        AStarPath aStar = new AStarPath(bestTile.getxPos(), bestTile.getyPos(), tiles);
        return aStar.aStar((int) robotPosition.getX(), (int) robotPosition.getY(), lava);
    }

    /**
     * Saves human if in range.
     *
     * @param human - to be saved.
     * @return - if human is in range.
     */
    public boolean humanInRange(Human human){
        double distanceToHuman = ManhattanDistance.calc(tiles[(int) robotPosition.getX()][(int) robotPosition.getY()], (int) human.getPosition().getX(), (int) human.getPosition().getY());
        if(distanceToHuman == 1.0 && humanInDanger){
            human.gotSaved();
            humanInDanger = false;
            System.out.println("Human saved!");
            return true;
        }
        return false;
    }

    /**
     * Generating a new path for intervening.
     *
     * @param humanX - human's X coordinate.
     * @param humanY - human's Y coordinate.
     * @return - the best tile for intervening.
     */
    private Tile generatePathCloseToLava(int humanX, int humanY){
        ArrayList<Tile> floorNextToLava = new ArrayList<>();
        for(Direction direction : Direction.values()){
            Tile tile = nearestLava.getSuccessor(tiles, direction);
            if(!(tile instanceof Lava)){
                floorNextToLava.add(tile);
            }
        }

        double distance = Double.MAX_VALUE;
        Tile bestTile = null;
        for(Tile tile : floorNextToLava){
           double tempDistance = ManhattanDistance.calc(tile, humanX, humanY);
            if(distance > tempDistance){
                distance = tempDistance;
                bestTile = tile;
            }
        }
        return bestTile;
    }

}

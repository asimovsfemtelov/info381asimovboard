package main;

import entities.*;
import pathing.AStarPath;
import world.Board;
import world.Tile;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The Simulation program implements a simulation with human(s) and a robot with a end goal on the 2D board.
 * Asimov's laws of robotics have been implemented in the robot, such that the robot will try to save any human
 * being in danger.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class Simulation {

    private Board board;
    private Entity human, robot;
    private AStarPath aStarHuman, aStarRobot;
    private List<Entity> entities;

    /**
     * Constructor
     *
     * Initializes the board and entities, setting paths to original goal.
     * Starts tick to run the simulation.
     */
    public Simulation(){
        board = new Board();
        entities = new ArrayList<>();

        human = new Human("X", 6, 11);
        robot = new AsimovRobot("R", board.getTileHolder(), 4, 11);

        aStarHuman = new AStarPath(((Human) human).getGoalX(), ((Human) human).getGoalY(), board.getTileHolder());
        List<Tile> humPath = aStarHuman.aStar(0,2, null);
        ((Human) human).setPath(humPath);

        aStarRobot = new AStarPath(((AsimovRobot) robot).getGoalX(), ((AsimovRobot) robot).getGoalY(), board.getTileHolder());
        List<Tile> robotPath = aStarRobot.aStar(7,2, Board.getLavaTiles());
        ((AsimovRobot) robot).setPath(robotPath);


        board.addEntity(human, 0, 2);
        board.addEntity(robot, 7, 2);

        // Setting colors to robot's goal tile (green) and human's goal tile(yellow)
        board.getTileHolder()[((Human) human).getGoalX()][((Human) human).getGoalY()].setBackground(Color.YELLOW);
        board.getTileHolder()[((AsimovRobot) robot).getGoalX()][((AsimovRobot) robot).getGoalY()].setBackground(Color.GREEN);

        entities.add(human);
        entities.add(robot);
        board.updateGraphics();

        tick(entities);

    }

    /**
     * The tick method represent a loop of 'rounds' or 'turns' in the simulation
     * where the human moves, then the robot does and evaluation and moves accordingly.
     *
     * @param entities - list of all entities involved in the game.
     */
    public void tick(List<Entity> entities){
        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        boolean running = true;
        final double ns = 1000000000.0 / 1.0;
        double delta = 0;
        while(running){
            long now = System.nanoTime();
            delta += (now-lastTime) / ns;
            lastTime = now;
            while(delta >= 1){
                delta--;
                for(Entity entity : entities) {
                    if (entity instanceof Human) {
                        if (((Human) entity).isDead()) {
                            // is dead
                        } else if(((Human) entity).isSaved()){
                            // is saved
                        } else {
                            ((Human) entity).followPath(board.getTileHolder());
                            if(entity.getPosition().getX() == ((Human) entity).getGoalX()
                                    && entity.getPosition().getY() == ((Human) entity).getGoalY()){
                                System.out.println("Human reached goal!");
                                ((Human) entity).gotSaved();
                            }
                        }
                    } else if(entity instanceof AsimovRobot){
                        ((AsimovRobot) entity).evaluate();
                        ((AsimovRobot) entity).followPath(board.getTileHolder());
                        ((AsimovRobot) entity).saveHuman();

                        if(((AsimovRobot) entity).getPosition().getX() == ((AsimovRobot) entity).getGoalX()
                                && ((AsimovRobot) entity).getPosition().getY() == ((AsimovRobot) entity).getGoalY()){
                            running = false;
                            System.out.println("Simulation is done.");
                        }
                    }
                    board.updateGraphics();
                }
            }

            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
            }
        }
    }

    public static void main(String[] args){
        new Simulation();
    }
}

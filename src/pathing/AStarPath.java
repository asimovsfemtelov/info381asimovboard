package pathing;

import entities.AsimovRobot;
import entities.Human;
import utils.ManhattanDistance;
import world.Board;
import world.Tile;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * AStarPath is used for pathfinding. It is an implementation of an A* algorithm.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class AStarPath {

    private PriorityQueue<Tile> openQueue;
    private HashSet<Tile> closed;
    private int goalX, goalY;
    private Tile[][] map;
    private final int MOVE_COST = 10;

    /**
     * Constructor for AStarPath.
     *
     * @param goalX - X coordinate for goal.
     * @param goalY - Y coordinate for goal.
     * @param map - tiles from the 2D board.
     */
    public AStarPath(int goalX, int goalY, Tile[][] map){
        this.goalX = goalX;
        this.goalY = goalY;
        this.map = map;
        setHeuristics();
        resetTileGValue();
    }

    /**
     * Resets G-values of tiles.
     */
    private void resetTileGValue(){
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map.length; j++){
                map[i][j].setG(Double.MAX_VALUE);
            }
        }
    }

    /**
     * The A* algorithm.
     *
     * @param x - entity's x coordinate
     * @param y - entity's y coordinate
     * @param closedSet - lava tiles
     * @return - List of tiles which will be the path from start to goal.
     */
    public List<Tile> aStar(int x, int y, HashSet<Tile> closedSet) {
        Map<Tile, Tile> path = new HashMap<>();
        this.closed = new HashSet<>();
        if(closedSet != null){
            for(Tile tile : closedSet){
                closed.add(tile);
            }
        }

        addEntitiesToClosed();

        openQueue = new PriorityQueue<Tile>(11, new TileComparator());
        Tile start = map[x][y];
        start.setG(0);
        start.setF(start.getH() + start.getG());
        openQueue.add(start);

        while(!openQueue.isEmpty()){
            Tile current = openQueue.poll();

            if(current.equals(map[goalX][goalY])) {
                return path(path, map[goalX][goalY]);
            }

            closed.add(current);
            for(Tile successor : findSuccessors(current)){
                if(closed.contains(successor)){
                    continue;
                }

                double tempG = current.getG() + MOVE_COST;
                if(tempG < successor.getG()){
                    successor.setG(tempG);
                    successor.setF(successor.getH() + successor.getG());

                    path.put(successor, current);
                    if(!openQueue.contains(successor)){
                        openQueue.add(successor);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Adds tiles occupied by entities to closed.
     */
    private void addEntitiesToClosed(){
        for(int i = 0; i < Board.ROWS; i++){
            for(int j = 0; j < Board.COLUMNS; j++){
                for(Component component : map[i][j].getComponents()){
                    if(component instanceof Human || component instanceof AsimovRobot){
                        closed.add(map[i][j]);
                    }
                }
            }
        }
    }

    /**
     * Sets heuristics for tiles.
     */
    public void setHeuristics(){
        for(int i = 0; i < Board.ROWS; i++){
            for(int j = 0; j < Board.COLUMNS; j++){
                map[i][j].setH(calcHeuristic(map[i][j]));
            }
        }
    }

    /**
     * Generates a path as a List.
     *
     * @param path - map of tiles with previous (key) and current (value) tile.
     * @param goal - goal tile.
     * @return
     */
    private List<Tile> path(Map<Tile, Tile> path, Tile goal) {
        List<Tile> pathList = new ArrayList<>();
        pathList.add(goal);
        while (path.containsKey(goal)) {
            goal = path.get(goal);
            pathList.add(goal);
        }
        Collections.reverse(pathList);
        pathList.remove(0);
        return pathList;
    }

    /**
     * Calculating heuristics for tile.
     *
     * @param tile - tile used in calculation
     * @return - Manhattan distance from given tile to goal.
     */
    public double calcHeuristic(Tile tile){
        return ManhattanDistance.calc(tile, goalX, goalY);

    }

    /**
     * Comparator for tiles used in PriorityQueue.
     */
    public class TileComparator implements Comparator<Tile> {
        @Override
        public int compare(Tile t1, Tile t2) {
            if(t1.getF() > t2.getF()) return 1;
            if(t2.getF() > t1.getF()) return -1;
            return 0;
        }
    }

    /**
     * Finds all possible successors for a tile.
     *
     * @param parent current tile.
     * @return List of all possible tiles to move to.
     */
    public List<Tile> findSuccessors(Tile parent){
        List<Tile> successors = new ArrayList<>();

        for(Direction direction : Direction.values()){
            Tile successor = parent.getSuccessor(map, direction);
            if(successor != null) {
                successor.setParent(parent);
                successors.add(successor);
            }
        }
        return successors;
    }
}

package utils;

import world.Tile;

/**
 * ManhattanDistance is an utility class for calculating Manhattan distance.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class ManhattanDistance {
    public static double calc(Tile tile, int goalX, int goalY){
        return (Math.abs(tile.getxPos() - goalX)) + (Math.abs(tile.getyPos() - goalY));
    }
}

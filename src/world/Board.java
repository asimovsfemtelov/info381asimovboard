package world;

import entities.AsimovRobot;
import entities.Entity;
import entities.Human;

import javax.swing.*;
import java.awt.*;
import java.util.HashSet;

/**
 * Board is a 2D representation of the board used in the simulation.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class Board extends JFrame {

    public final static int ROWS = 15;
    public final static int COLUMNS = 15;
    public static Tile[][] tileHolder = new Tile[ROWS][COLUMNS];
    private JPanel panel;

    /**
     * Constructor for Board.
     *
     * Initializing the frame and everything contained in the frame.
     */
    public Board(){
        setTitle("Asimov implementation");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(590,600));
        setLocationRelativeTo(null); // centers frame

        panel = new JPanel();
        panel.setLayout(new GridLayout(ROWS,COLUMNS));

        initializeBoard();

        add(panel);

        setVisible(true);
    }

    /**
     * Updating graphics.
     */
    public void updateGraphics(){
        validate();
        repaint();
    }

    /**
     * Initializing the floor and lava tiles.
     */
    public void initializeBoard(){
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLUMNS; j++){
                Tile tile;

                if ((j == 9 && i == 4) || (j == 10 && i == 4) || (j == 9 && i == 5) || j == 10 && i == 5){
                    tile = new Lava(i, j);
                } else {
                    tile = new Floor(i, j);
                }

                tileHolder[i][j] = tile;
                panel.add(tileHolder[i][j]);
            }
        }
    }

    /**
     * Getting all lava tiles.
     *
     * @return - a HashSet of all lava tiles
     */
    public static HashSet<Tile> getLavaTiles(){
        HashSet<Tile> lavaTiles = new HashSet<Tile>();
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLUMNS; j++){
                if(tileHolder[i][j] instanceof Lava){
                    lavaTiles.add(tileHolder[i][j]);
                }
            }
        }
        return lavaTiles;
    }

    /**
     * Adds an entity to the board.
     *
     * @param entity - the entity to be added.
     * @param x - x coordinate to place entity.
     * @param y - y coordinate to place entity.
     */
    public void addEntity(Entity entity, int x, int y){
        entity.setPosition(x,y);
        if(entity instanceof Human) {
            tileHolder[x][y].add((Human) entity);
        }  else if (entity instanceof AsimovRobot){
            tileHolder[x][y].add((AsimovRobot) entity);
        }
    }

    /**
     * Getter for 2D board.
     *
     * @return array of tiles in board.
     */
    public Tile[][] getTileHolder(){
        return tileHolder;
    }

}



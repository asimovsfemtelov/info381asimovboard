package world;

import java.awt.*;

/**
 * Floor is a representation of the floor tiles used in the 2D board.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class Floor extends Tile {

    /**
     * Constructor for Floor.
     *
     * @param xPos - x coordinate.
     * @param yPos - y coordinate.
     */
    public Floor(int xPos, int yPos){
        super(Color.LIGHT_GRAY, xPos, yPos);
    }

}


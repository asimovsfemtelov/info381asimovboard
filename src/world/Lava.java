package world;

import java.awt.*;

/**
 * Lava is a representation of the lava tiles used in the 2D board.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public class Lava extends Tile {

    /**
     * Constructor for Lava.
     *
     * @param xPos - x coordinate.
     * @param yPos - y coordinate.
     */
    public Lava(int xPos, int yPos){
        super(Color.RED, xPos, yPos);
    }
}

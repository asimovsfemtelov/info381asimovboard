package world;

import pathing.Direction;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * Tile is an abstract superclass for all tiles in the 2D board.
 *
 * @author Mateo Caycedo Alvarez
 * @author Øyvind Svenning Berge
 * @author Audun Svensson Berget
 * @author Eirin Skaar Bjørknes
 * @author Dag Vegard Kollstrøm Johnsen
 * @author Fredrik Madsen
 *
 * @version 1.0
 */
public abstract class Tile extends JPanel {

    private double f, g, h; // final, goal, heuristic
    private Tile parent;    // previous tile for pathfinding
    private int xPos, yPos;

    /**
     * Constructor for Tile.
     *
     * @param color - color of tile.
     * @param xPos  - x coordinate.
     * @param yPos - y coordinate.
     */
    public Tile(Color color, int xPos, int yPos) {
        super();
        setSize(new Dimension(10, 10));
        setOpaque(true);
        setBorder(LineBorder.createBlackLineBorder());
        setBackground(color);
        setVisible(true);

        this.xPos = xPos;
        this.yPos = yPos;

        setF(0);
        setH(0);
        setG(Double.MAX_VALUE);
    }

    /**
     * Getting all possible successors for a tile.
     *
     * @param map - the 2D array of tiles.
     * @param direction - The direction of successor tile.
     * @return - the successor tile.
     */
    public Tile getSuccessor(Tile[][] map, Direction direction) {
        Tile successor = null;
        try {
            switch (direction) {
                case NORTH:
                    successor = map[xPos - 1][yPos];
                    break;
                case SOUTH:
                    successor = map[xPos + 1][yPos];
                    break;
                case WEST:
                    successor = map[xPos][yPos - 1];
                    break;
                case EAST:
                    successor = map[xPos][yPos + 1];
                    break;
            }
        } catch (IndexOutOfBoundsException e) {
        }
        return successor;
    }

    /**
     * Setting Tile's parent.
     *
     * @param parent - parent of tile.
     */
    public void setParent(Tile parent) {
        this.parent = parent;
    }

    /**
     * Getting Tile's parent.
     *
     * @return parent - parent of tile.
     */
    public Tile getTileParent() {
        return parent;
    }

    /**
     * Getting X coordinate.
     *
     * @return xPos - X coordinate.
     */
    public int getxPos() {
        return xPos;
    }

    /**
     * Getting Y coordinate.
     *
     * @return yPos - Y coordinate.
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * Getting final value.
     *
     * @return f - final value.
     */
    public double getF() {
        return f;
    }

    /**
     * Setting final value.
     *
     * @return f - final value.
     */
    public void setF(double f) {
        this.f = f;
    }

    /**
     * Getting goal value.
     *
     * @return g - goal value.
     */
    public double getG() {
        return g;
    }

    /**
     * Setting goal value.
     *
     * @return g - goal value.
     */
    public void setG(double g) {
        this.g = g;
    }

    /**
     * Getting heuristic value.
     *
     * @return h - heuristic value.
     */
    public double getH() {
        return h;
    }

    /**
     * Setting heuristic value.
     *
     * @return h - heuristic value.
     */
    public void setH(double h) {
        this.h = h;
    }
}

